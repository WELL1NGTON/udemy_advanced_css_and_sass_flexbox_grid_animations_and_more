<!-- markdownlint-disable MD024 -->

# Advanced CSS and Sass: Flexbox, Grid, Animations and More

[![Udemy](https://logos-world.net/wp-content/uploads/2021/11/Udemy-Logo-2021-present-700x394.png)](https://www.udemy.com/)

> The most advanced and modern CSS course on the internet: master flexbox, CSS
> Grid, responsive design, and so much more.
>
> What you'll learn:
>
> - Tons of modern CSS techniques to create stunning designs and effects
> - Advanced CSS animations with @keyframes, animation and transition
> - How CSS works behind the scenes: the cascade, specificity, inheritance, etc.
> - CSS architecture: component-based design, BEM, writing reusable code, etc.
> - Flexbox layouts: build a huge real-world project with flexbox
> - CSS Grid layouts: build a huge real-world project with CSS Grid
> - Using Sass in real-world projects: global variables, architecting CSS,
>   managing media queries, etc.
> - Advanced responsive design: media queries, mobile-first vs desktop-first, em
>   vs rem units, etc.
> - Responsive images in HTML and CSS for faster pageloads
> - SVG images and videos in HTML and CSS: build a background video effect
> - The NPM ecosystem: development workflows and building processes
> - Get friendly and fast support in the course Q&A
> - Downloadable lectures, code and design assets for all projects

Udemy Course:
[Advanced CSS and Sass: Flexbox, Grid, Animations and More!](https://www.udemy.com/course/advanced-css-and-sass)

## Instructors

### Jonas Schmedtmann

[Course page](https://www.udemy.com/user/jonasschmedtmann/)

#### Links

- [Website](http://jonas.io/)
- [Twitter](https://twitter.com/jonasschmedtman)
- [Youtube](https://www.youtube.com/channel/UCNsU-y15AwmU2Q8QTQJG1jw)

<!--
## Certificado de Conclusão Udemy

[![Certificado Udemy](images/UC-38ba380b-a81e-41c1-8ddb-8bad1d8e7abb.jpg)](http://ude.my/UC-38ba380b-a81e-41c1-8ddb-8bad1d8e7abb)
-->

## Material & Resources

Github starter code:
[https://github.com/jonasschmedtmann/advanced-css-course](https://github.com/jonasschmedtmann/advanced-css-course)

Discord Group: [https://discord.gg/uhMkpf4](https://discord.gg/uhMkpf4)

Instructor's resources page:
[https://codingheroes.io/resources/](https://codingheroes.io/resources/)

## Course content

10 sections • 126 lectures • 28h 9m total length

### Welcome, Welcome, Welcome! - 3 lectures • 7min

- [x] Welcome to the Most Advanced CSS Course Ever! 03:54
- [x] READ BEFORE YOU START! 00:34
- [x] Setting up Our Tools 02:52

### Natours Project — Setup and First Steps (Part 1) - 7 lectures • 1hr 36min

- [x] Section Intro 00:44
- [x] Project Overview 05:42
- [x] Building the Header - Part 1 24:23
- [x] Building the Header - Part 2 17:06
- [x] Creating Cool CSS Animations 19:19
- [x] Building a Complex Animated Button - Part 1 15:57
- [x] Building a Complex Animated Button - Part 2 12:51

### How CSS Works: A Look Behind the Scenes - 11 lectures • 1hr 30min

- [x] Section Intro 01:38
- [x] Three Pillars of Writing Good HTML and CSS (Never Forget Them!) 04:21
- [x] How CSS Works Behind the Scenes: An Overview 04:32
- [x] How CSS is Parsed, Part 1: The Cascade and Specificity 12:33
- [x] Specificity in Practice 05:55
- [x] How CSS is Parsed, Part 2: Value Processing 15:44
- [x] How CSS is Parsed, Part 3: Inheritance 04:05
- [x] Converting px to rem: An Effective Workflow 14:01
- [x] How CSS Renders a Website: The Visual Formatting Model 12:45
- [x] CSS Architecture, Components and BEM 09:17
- [x] Implementing BEM in the Natours Project 05:14

### Introduction to Sass and NPM - 8 lectures • 1hr 33min

- [x] Section Intro 01:02
- [x] What is Sass? 04:58
- [x] First Steps with Sass: Variables and Nesting 25:22
- [x] First Steps with Sass: Mixins, Extends and Functions 17:05
- [x] A Brief Introduction to the Command Line 11:21
- [x] NPM Packages: Let's Install Sass Locally 13:48
- [x] NPM Scripts: Let's Write and Compile Sass Locally 13:10
- [x] The Easiest Way of Automatically Reloading a Page on File Changes 06:22

### Natours Project — Using Advanced CSS and Sass (Part 2) - 24 lectures • 7hr 45min

- [x] Section Intro 00:55
- [x] Converting Our CSS Code to Sass: Variables and Nesting 11:17
- [x] Implementing the 7-1 CSS Architecture with Sass 17:34
- [x] Review: Responsive Design Principles and Layout Types 07:48
- [x] Building a Custom Grid with Floats 37:03
- [x] Building the About Section - Part 1 23:25
- [x] Building the About Section - Part 2 21:51
- [x] Building the About Section - Part 3 17:30
- [x] Building the Features Section 30:19
- [x] Building the Tours Section - Part 1 25:28
- [x] Building the Tours Section - Part 2 31:00
- [x] Building the Tours Section - Part 3 16:36
- [x] Building the Stories Section - Part 1 19:40
- [x] Building the Stories Section - Part 2 14:14
- [x] Building the Stories Section - Part 3 13:49
- [x] Building the Booking Section - Part 1 18:19
- [x] Building the Booking Section - Part 2 18:37
- [x] Building the Booking Section - Part 3 23:10
- [x] Building the Footer 15:57
- [x] Building the Navigation - Part 1 29:37
- [x] Building the Navigation - Part 2 13:13
- [x] Building the Navigation - Part 3 15:36
- [x] Building a Pure CSS Popup - Part 1 25:13
- [x] Building a Pure CSS Popup - Part 2 16:55

### Natours Project — Advanced Responsive Design (Part 3) - 13 lectures • 3hr 28min

- [x] Section Intro 01:00
- [x] Mobile-First vs Desktop-First and Breakpoints 17:35
- [x] Let's Use the Power of Sass Mixins to Write Media Queries 28:08
- [x] Writing Media Queries - Base, Typography and Layout 26:39
- [x] Writing Media Queries - Layout, About and Features Sections 12:33
- [x] Writing Media Queries - Tours, Stories and Booking Sections 20:26
- [x] An Overview of Responsive Images 05:41
- [x] Responsive Images in HTML - Art Direction and Density Switching 10:14
- [x] Responsive Images in HTML - Density and Resolution Switching 17:21
- [x] Responsive Images in CSS 11:42
- [x] Testing for Browser Support with @supports 21:42
- [x] Setting up a Simple Build Process with NPM Scripts 20:55
- [x] Wrapping up the Natours Project: Final Considerations 13:58

### Trillo Project — Master Flexbox! - 22 lectures • 5hr

- [x] Section Intro 01:03
- [x] Why Flexbox: An Overview of the Philosophy Behind Flexbox 06:31
- [x] A Basic Intro to Flexbox: The Flex Container 15:08
- [x] A Basic Intro to Flexbox: Flex Items 10:34
- [x] A Basic Intro to Flexbox: Adding More Flex Items 06:14
- [x] Project Overview 07:14
- [x] Defining Project Settings and Custom Properties 13:40
- [x] Building the Overall Layout 12:00
- [x] Building the Header - Part 1 17:59
- [x] Building the Header - Part 2 14:48
- [x] Building the Header - Part 3 12:26
- [x] Building the Navigation - Part 1 11:36
- [x] Building the Navigation - Part 2 16:50
- [x] Building the Hotel Overview - Part 1 15:18
- [ ] Building the Hotel Overview - Part 2 15:26
- [ ] Building the Description Section - Part 1 12:53
- [ ] Building the Description Section - Part 2 19:38
- [ ] Building the User Reviews Section 22:52
- [ ] Building the CTA Section 16:54
- [ ] Writing Media Queries - Part 1 19:27
- [ ] Writing Media Queries - Part 2 16:27
- [ ] Wrapping up the Trillo Project: Final Considerations 14:45

### A Quick Introduction to CSS Grid Layouts - 16 lectures • 2hr 23min

- [ ] Section Intro 00:59
- [ ] Why CSS Grid: A Whole New Mindset 05:15
- [ ] Quick Setup for This Section 02:11
- [ ] Creating Our First Grid 12:15
- [ ] Getting Familiar with the fr Unit 10:00
- [ ] Positioning Grid Items 07:26
- [ ] Spanning Grid Items 09:48
- [ ] Grid Challenge 02:48
- [ ] Grid Challenge: A Basic Solution 11:08
- [ ] Naming Grid Lines 09:40
- [ ] Naming Grid Areas 11:03
- [ ] Implicit Grids vs. Explicit Grids 10:39
- [ ] Aligning Grid Items 09:57
- [ ] Aligning Tracks 12:59
- [ ] Using min-content, max-content and the minmax() function 16:19
- [ ] Responsive Layouts with auto-fit and auto-fill 10:57

### Nexter Project — Master CSS Grid Layouts! - 20 lectures • 4hr 43min

- [ ] Project Overview and Setup 11:31
- [ ] Building the Overall Layout - Part 1 15:33
- [ ] Building the Overall Layout - Part 2 24:31
- [ ] Building the Features Section - Part 1 13:01
- [ ] Building the Features Section - Part 2 20:27
- [ ] Building the Story Section - Part 1 18:00
- [ ] Building the Story Section - Part 2 11:38
- [ ] Building the Homes Section - Part 1 15:46
- [ ] Building the Homes Section - Part 2 18:51
- [ ] Building the Gallery - Part 1 15:24
- [ ] Building the Gallery - Part 2 12:27
- [ ] Building the Footer 12:33
- [ ] Building the Sidebar 05:08
- [ ] Building the Header - Part 1 16:15
- [ ] Building the Header - Part 2 09:51
- [ ] Building the Realtors Section 11:22
- [ ] Writing Media Queries - Part 1 17:12
- [ ] Writing Media Queries - Part 2 18:38
- [ ] Browser Support for CSS Grid 08:35
- [ ] Wrapping up the Nexter Project: Final Considerations 06:44

### That's It, Everyone! - 2 lectures • 3min

- [ ] See You Next Time, CSS Master! 02:28
- [ ] My Other Courses + Updates 00:54
