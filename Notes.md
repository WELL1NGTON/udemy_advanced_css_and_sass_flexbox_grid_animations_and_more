# Notes

## CSS Reset

Simple CSS reset that he uses:

```css
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
```

[`box-sizing: border-box;`](https://www.w3schools.com/cssref/css3_pr_box-sizing.php)
The width and height properties (and min/max properties) includes content,
padding and border

obs.: he don't use normalize.css anymore as browsers are getting more consistent
on rendering the page the same way

Default body natours project:

```css
body {
  font-family: 'Lato', sans-serif;
  font-weight: 400;
  font-size: 16px;
  line-height: 1.7;
  color: #777;
  padding: 30px;
}
```

`height: 95vh;` 95% of the viewport height

## Creating Cool CSS Animations (lecture 8)

The instructor explains that there is a small "shake" hat sometimes happens in
animations and nobody knows why. He says that the best way to fix it is to use
`backface-visibility: hidden;` in the element that is being animated.

## Implementing BEM in the Natours Project (lecture 21)

BEM = Block Element Modifier

```html
<div class="block">
  <div class="block__element"></div>
  <div class="block__element block__element--modifier"></div>
</div>
```

```css
.block {
}
.block__element {
}
.block__element--modifier {
}
```

## First Steps with Sass: Variables and Nesting (lecture 24)

Done in codepen:
[https://codepen.io/well1ngton/pen/OJrwgVg](https://codepen.io/well1ngton/pen/OJrwgVg)

## First Steps with Sass: Mixins, Extends and Functions (lecture 25)

Done in codepen:
[https://codepen.io/well1ngton/pen/zYyLdbE](https://codepen.io/well1ngton/pen/zYyLdbE)

## The Easiest Way of Automatically Reloading a Page on File Changes (lecture 29)

Some changes:

- node-sass used in the course is old, I used the new version so in the
  main.scss I had to change "rgb(...)" to "rgba(...)"
- the instructor installed "live-server" as global, I installed it as dev
  dependency and added a script to run it (see package.json)

## Building the About Section - Part 3 (lecture 37)

In `_composition.scss`, the instructor uses a square outline on the images,
however the way he does it, only works on Safari. I found a way to do it that
works on Chrome and Firefox too:

```scss
.composition {
  &__photo {
    // ...same as the lecture
    transition: all 0.2s, border-radius 0s;

    &:hover {
      // ...same as the lecture
      border-radius: 0;
    }
  }
}
```

## Building the Features Section (lecture 38)

Fonts from [linea.io](https://linea.io/).

Obs.: there is no more "Download all sets", so I downloaded "Free Basic Icons",
I hope it works. `¯\_(ツ)_/¯`

## Building the Booking Section - Part 2 (lecture 46)

The `&::-webkit-input-placeholder` selector didn't work in Firefox, so I used
`&::placeholder` instead and it seems to work in both chrome and firefox, but I
also kept the `&::-webkit-input-placeholder` and left a comment on the
`_form.scss` file.
